package com.gojolo.gamescreenlock;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends Activity {
	 BroadcastReceiver mReceiver;
static EditText edit1,edit2;
static Button btn,on;
static int ox=0;
static int eo=0;
private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		AdRequest adRequest = new AdRequest.Builder().build(); 
	    interstitial = new InterstitialAd(MainActivity.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/5596199445");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	final	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());	
	final	SharedPreferences.Editor editor = preferences.edit();
	   final EditText edit1 = (EditText)findViewById(R.id.edit);
	   final EditText edit2 = (EditText)findViewById(R.id.edit2);
		InputFilter[] FilterArray = new InputFilter[1]; 
		FilterArray[0] = new InputFilter.LengthFilter(4); 
		edit1.setFilters(FilterArray);
		edit2.setFilters(FilterArray);
	  final  Button btn = (Button)findViewById(R.id.btn);
	  final Button on = (Button)findViewById(R.id.on);
   final   ComponentName receiver = new ComponentName(this, LockScreen.class);
   final   PackageManager pm = this.getPackageManager();
   ox=preferences.getInt("lock", 0);
	   if(ox==0)
	   {				 btn.setVisibility(View.VISIBLE);
	     edit1.setVisibility(View.VISIBLE);
	     edit2.setVisibility(View.VISIBLE);
	     on.setVisibility(View.GONE);}
	   else
	   {
		     btn.setVisibility(View.GONE);
		     edit1.setVisibility(View.GONE);
		     edit2.setVisibility(View.GONE);
		     on.setVisibility(View.VISIBLE);
	   }
	    btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(edit1.getText().toString().matches("") ||edit2.getText().toString().matches(""))
				{ 
					Toast.makeText(getApplication(),"password can not be empty",Toast.LENGTH_LONG).show();
				}
				else if(!edit1.getText().toString().equals(edit2.getText().toString()))
				{
					Toast.makeText(getApplication(),"Passwords do not match",Toast.LENGTH_LONG).show();
				}
				else{
				      pm.setComponentEnabledSetting(receiver,
				              PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				              PackageManager.DONT_KILL_APP);
					 Intent  kilit = new  Intent(MainActivity.this,LockScreen.class);
				     kilit.putExtra("pass",Integer.parseInt(edit2.getText().toString()));
				     startActivity(kilit);
				     finish();
				     editor.putInt("lock",1);
						editor.commit();
				}
			}
		});
	    on.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			      pm.setComponentEnabledSetting(receiver,
			              PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
			              PackageManager.DONT_KILL_APP);
			 	 btn.setVisibility(View.VISIBLE);
			     edit1.setVisibility(View.VISIBLE);
			     edit2.setVisibility(View.VISIBLE);
			     on.setVisibility(View.GONE);
			}
		});
        try{
     // initialize receiver
     Intent intent = new Intent(this,MyService.class);
     startService(intent);


        }
       catch(Exception e)
       {
    	   
       }
		
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
