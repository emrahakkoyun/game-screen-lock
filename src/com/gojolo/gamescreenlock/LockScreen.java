package com.gojolo.gamescreenlock;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
public class LockScreen extends Activity implements View.OnTouchListener{
static int pass;
boolean movee;
ImageView ball;
Button password;
static float nx,ny;
static float ox,oy;
static int[] k=new int[4];
int pcount=0,bcount=0,i,j,l;
int[] bpass=new int[4];
static int screenHeight,screenWidth;
boolean isLock=false;
private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lock_screen); 
		AdRequest adRequest = new AdRequest.Builder().build(); 
	    interstitial = new InterstitialAd(LockScreen.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/7072932646");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());	
		SharedPreferences.Editor editor = preferences.edit();
	       this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		TextView Clock =(TextView)findViewById(R.id.Clock);
		Calendar c = Calendar.getInstance();
        int minutes = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR);
        String time = hour+":"+minutes;
        Clock.setText(time);
		password=(Button)findViewById(R.id.password);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		ball = (ImageView)findViewById(R.id.ball);
		ball.bringToFront();
		if(getIntent().getExtras()!=null)
	    {Bundle extras = getIntent().getExtras();
		editor.putInt("pass",extras.getInt("pass"));
		editor.commit();
	    }
		pass=preferences.getInt("pass", 0000);
		 i=pass;
		 l=pass;
		 while(i>0)
		{
			 i=i/10;
			 pcount++;
		}
		 j=pcount-1;
		 while(l>0)
			{
				 k[j]=l%10;
				 l=l/10;
				 j--;
			}
		 RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth/12),(screenHeight/12));
	   	  layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		    ball.setLayoutParams(layoutParams);
			 RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) Math.round(screenWidth/1.8),(int) Math.round(screenHeight/9));
		   	  layoutParams2.topMargin=(int) Math.round(screenHeight/1.36);
		   	  layoutParams2.leftMargin= (int) Math.round(screenHeight/7);
			  password.setLayoutParams(layoutParams2);
			  password.setTextSize((screenWidth/20));
			  if(pcount==1)
			  {password.setText("?");}
			  if(pcount==2)
			  {password.setText("? ?");}
			  if(pcount==3)
			  {password.setText("? ? ?");}
			  if(pcount==4)
			  password.setText("? ? ? ?");
			 Clock.setTextSize(screenHeight/15);
		   ball.setOnTouchListener(this);
		 movee=true;
	}
	public boolean onTouch(View view, MotionEvent event) {
	    switch (event.getAction() & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
                movee=true;
               break;
	        case MotionEvent.ACTION_UP:
	        	movee=false;
	        	moveball(ball,nx-ox,ny-oy);
	        	ox=nx; oy=ny;         
	            break;
	        case MotionEvent.ACTION_MOVE:
	        	if(movee)
	            {
	        	nx=event.getRawX()-ball.getWidth()/2+20;
	        	ny=event.getRawY()-ball.getHeight()/2-20;
	        	if(ny>=31&&ny<1072&&nx>=62&&nx<=622)
	        	{ball.setX(nx);
	        	ball.setY(ny);
	        	}
	            }

	            break;
	    }

	    return true;
	}
	public void center(View view,int number)
	{
		bpass[bcount]=number;
		 view.setX(screenWidth/2);
		 view.setY(screenHeight/2);
		 if(pcount==1)
		 { if(bcount==0)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0]));
		 }
		 bcount++;
		 if(bcount==1)
			{ bcount=0;
			if((k[0]==bpass[0]))
			{
				finish();
			}
			}
		 }
		 if(pcount==2)
		 { if(bcount==0)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+" ? ");
		 }
		 if(bcount==1)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+String.valueOf(bpass[1]));
		 }
		 bcount++;
		 if(bcount==2)
			{ bcount=0;
			if((k[0]==bpass[0])&&(k[1]==bpass[1]))
			{
				finish();
			}
			}
		 }
		 if(pcount==3)
		 { if(bcount==0)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+" ? "+" ? ");
		 }
		 if(bcount==1)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+String.valueOf(bpass[1])+" ? ");
		 }
		 if(bcount==2)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+String.valueOf(bpass[1])+String.valueOf(bpass[2]));
		 }
		 bcount++;
		 if(bcount==3)
			{ bcount=0;
			if((k[0]==bpass[0])&&(k[1]==bpass[1])&&(k[2]==bpass[2]))
			{
				finish();
			}
			}
		 }
		 if(pcount==4)
		 { if(bcount==0)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+" ? "+" ? "+" ? ");
		 }
		 if(bcount==1)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+String.valueOf(bpass[1])+" ? "+" ? ");
		 }
		 if(bcount==2)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+String.valueOf(bpass[1])+String.valueOf(bpass[2])+" ? ");
		 }
		 if(bcount==3)
		 {
			 password.setText("");
			 password.setText(String.valueOf(bpass[0])+String.valueOf(bpass[1])+String.valueOf(bpass[2])+String.valueOf(bpass[3]));
		 }
		 bcount++;
		 if(bcount==4)
			{ bcount=0;
			if((k[0]==bpass[0])&&(k[1]==bpass[1])&&(k[2]==bpass[2])&&(k[3]==bpass[3]))
			{
				finish();
			}
			}
		 }
	}
	public void moveball(final View view,float x,float y)
	{
		 if(ny+y<=31)
		{ y=31-ny;}
		if(ny+y>=1072)
		{ y=1072-ny;}
		if(nx+x<=62)
		{ x=62-nx;}
		if(nx+x>=622)
		{x=622-nx;}
		final float gx=x,gy=y;
		final TranslateAnimation  anim1 = new TranslateAnimation( 0,x,0,y);
	    anim1.setDuration(1000);
	    anim1.setFillEnabled(true);
	    view.startAnimation(anim1);
	    anim1.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	        
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    		   ball.setX(nx+gx);
		    		   ball.setY(ny+gy); 
		    		   if(((nx+gx)<=(screenWidth/7))&&((ny+gy)<=(screenHeight/15)))
		    		   {center(view,0);}
		    		   if(((nx+gx)>=(screenWidth/2.4))&&((nx+gx)<=(screenWidth/1.9))&&((ny+gy)<=(screenHeight/23)))
		    		   {center(view,1);}
		    		   if(((nx+gx)>=(screenWidth/1.26))&&((ny+gy)<=(screenHeight/13)))
		    		   {center(view,2);}
		    		   if(((nx+gx)>=(screenWidth/1.26))&&((ny+gy)>=(screenHeight/4.9))&&((ny+gy)<=(screenHeight/3.7)))
		    		   {center(view,3);}
		    		   if(((nx+gx)>=(screenWidth/1.26))&&((ny+gy)<=(screenHeight/1.36))&&((ny+gy)>=(screenHeight/1.5)))
		    		   {center(view,4);}
		    		   if(((nx+gx)>=(screenWidth/1.26))&&((ny+gy)>=(screenHeight/1.2)))
		    		   {center(view,5);}
		    		   if(((nx+gx)>=(screenWidth/2.4))&&((nx+gx)<=(screenWidth/1.9))&&((ny+gy)>=(screenHeight/1.2)))
		    		   {center(view,6);}
		    		   if(((nx+gx)<=(screenWidth/7))&&((ny+gy)>=(screenHeight/1.2)))
		    		   {center(view,7);}
		    		   if(((nx+gx)<=(screenWidth/7))&&((ny+gy)<=(screenHeight/1.36))&&((ny+gy)>=(screenHeight/1.5)))
		    		   {center(view,8);}
		    		   if(((nx+gx)<=(screenWidth/7))&&((ny+gy)>=(screenHeight/4.9))&&((ny+gy)<=(screenHeight/3.7)))
		    		   {center(view,9);}
		    		   
    	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
 });
	}

	class StateListener extends PhoneStateListener{
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            super.onCallStateChanged(state, incomingNumber);
            switch(state){
                case TelephonyManager.CALL_STATE_RINGING:
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    System.out.println("call Activity off hook");
                	finish();



                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
       return;
    }

    //only used in lockdown mode
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if ( (event.getKeyCode() == KeyEvent.KEYCODE_HOME)) {
            return true;
        }
        if( (event.getKeyCode()==KeyEvent.KEYCODE_BACK))
        {
            return true;
        }
        else
            return super.dispatchKeyEvent(event);
    }
    public void onDestroy(){
        super.onDestroy();
    }
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
	
}
