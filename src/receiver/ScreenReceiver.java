package receiver;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gojolo.gamescreenlock.LockScreen;

public class ScreenReceiver extends BroadcastReceiver  {
	public static boolean wasScreenOn = true;
	@Override
	  public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			wasScreenOn = false;
			Intent lockscreen = new Intent(context, LockScreen.class);
			lockscreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(lockscreen);
			
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			wasScreenOn = true;
			Intent lockscreen = new Intent(context,LockScreen.class);
			lockscreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Intent intent11 = new Intent(context, LockScreen.class);
            intent11.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent11);
		}
	  }
	
}
